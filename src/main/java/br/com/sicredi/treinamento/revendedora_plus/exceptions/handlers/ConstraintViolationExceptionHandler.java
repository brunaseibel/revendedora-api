package br.com.sicredi.treinamento.revendedora_plus.exceptions.handlers;

import br.com.sicredi.treinamento.revendedora_plus.exceptions.dto.ApiErrorDTO;
import br.com.sicredi.treinamento.revendedora_plus.exceptions.dto.ApiErrorDetailDTO;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ConstraintViolationExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ApiErrorDTO> handle(ConstraintViolationException ex) {
        return ResponseEntity.badRequest().body(process(ex));
    }

    private ApiErrorDTO process(ConstraintViolationException ex) {
        List<ApiErrorDetailDTO> details = ex.getConstraintViolations().stream()
                .map(e -> ApiErrorDetailDTO.builder()
                        .field(((PathImpl) e.getPropertyPath()).getLeafNode().getName())
                        .message(e.getMessage())
                        .build())
                .collect(Collectors.toList());

        return ApiErrorDTO.builder()
                .code("constraint_error")
                .detail("Erro na validação")
                .details(details)
                .build();
    }
}
