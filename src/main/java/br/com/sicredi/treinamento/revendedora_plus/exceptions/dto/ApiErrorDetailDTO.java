package br.com.sicredi.treinamento.revendedora_plus.exceptions.dto;

public class ApiErrorDetailDTO {
    private String field;
    private String message;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static ApiErrorDetailBuilder builder() {
        return new ApiErrorDetailBuilder();
    }

    public static final class ApiErrorDetailBuilder {
        private String field;
        private String message;

        private ApiErrorDetailBuilder() {
        }

        public ApiErrorDetailBuilder field(String field) {
            this.field = field;
            return this;
        }

        public ApiErrorDetailBuilder message(String message) {
            this.message = message;
            return this;
        }

        public ApiErrorDetailDTO build() {
            ApiErrorDetailDTO apiErrorDetailDTO = new ApiErrorDetailDTO();
            apiErrorDetailDTO.setField(field);
            apiErrorDetailDTO.setMessage(message);
            return apiErrorDetailDTO;
        }
    }
}
