package br.com.sicredi.treinamento.revendedora_plus.service;

import br.com.sicredi.treinamento.revendedora_plus.exceptions.FipeIndisponivelException;

import java.math.BigDecimal;

public interface FipeService {

  BigDecimal getValor(String modelo, Integer anoModelo) throws FipeIndisponivelException;
}
