package br.com.sicredi.treinamento.revendedora_plus.exceptions.dto;

import java.util.List;

public class ApiErrorDTO {

    private String code;
    private String detail;
    private List<ApiErrorDetailDTO> details;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public ApiErrorDTO(String code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public List<ApiErrorDetailDTO> getDetails() {
        return details;
    }

    public void setDetails(List<ApiErrorDetailDTO> details) {
        this.details = details;
    }


    public static ApiErrorDTOBuilder builder() {
        return new ApiErrorDTOBuilder();
    }

    public static final class ApiErrorDTOBuilder {
        private String code;
        private String detail;
        private List<ApiErrorDetailDTO> details;

        private ApiErrorDTOBuilder() {
        }

        public ApiErrorDTOBuilder code(String code) {
            this.code = code;
            return this;
        }

        public ApiErrorDTOBuilder detail(String detail) {
            this.detail = detail;
            return this;
        }

        public ApiErrorDTOBuilder details(List<ApiErrorDetailDTO> details) {
            this.details = details;
            return this;
        }

        public ApiErrorDTO build() {
            ApiErrorDTO apiErrorDTO = new ApiErrorDTO(code, detail);
            apiErrorDTO.setDetails(details);
            return apiErrorDTO;
        }
    }
}
